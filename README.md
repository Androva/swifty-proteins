# Swifty Proteins

Second, and final, project in the WeThinkCode_ Swift Module.

Swifty Proteins calls Protein Data Bank models from the RCSB.org API and
constructs and renders the proteins as 3D models using Apple's SceneKit.
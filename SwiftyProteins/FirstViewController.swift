//
//  ViewController.swift
//  SwiftyProteins
//
//  Created by Naledi MATUTOANE on 2019/12/02.
//  Copyright © 2019 Naledi MATUTOANE. All rights reserved.
//

import UIKit
import LocalAuthentication

class FirstViewController: UIViewController {
    @IBAction func button(_ sender: Any) {
        TouchIDAuth()
//        performSegue(withIdentifier: "proteinsList", sender: self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        ReadFile.readFile(fileName: "Ligands", fileExt: "txt")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is ProteinsTableViewController {
            _ = segue.destination as! ProteinsTableViewController
        }
    }
}

extension UIViewController {
    func showAlertController(_ message: String) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
    
    func TouchIDAuth() {
        let context = LAContext()
        
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) {
            let reason = "Authenticate with Touch ID"
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason, reply:
                {(success, error) in
                    // 4
                    DispatchQueue.main.sync {
                        if success {
                            self.performSegue(withIdentifier: "proteinsList", sender: self)
                        }
                        else {
                            self.showAlertController("Touch ID Authentication failed. Please try again.")
                        }
                    }
            })
        }
        else {
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "proteinsList", sender: self)
            }
        }
    }
}
